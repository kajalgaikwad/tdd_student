class Student < ApplicationRecord
	has_many :subjects
	validates_presence_of :student_name, :rollno
	validates_length_of :student_name, :maximum =>10
end
