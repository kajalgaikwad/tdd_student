class Subject < ApplicationRecord
	belongs_to :student
	validates_presence_of :subject_name, :student_id
	validates_length_of :subject_name, :maximum =>10
end
