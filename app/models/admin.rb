class Admin < ApplicationRecord
	validates_presence_of :first_name, :last_name, :email, :password
	validates_length_of :first_name, :last_name, :maximum =>10
end
