require 'rails_helper'

RSpec.describe Mark, type: :model do
	before(:each) do
  	@mark = Mark.create!(marks: "10", subject_id: "12")
  end
  describe 'validation tests' do
  	it 'ensure marks presence' do
  		@mark.marks = nil
  		expect(@mark).to_not be_valid
  	end

  	it 'ensure subject id presence' do
  		@mark.subject_id = nil
  		expect(@mark).to_not be_valid
  	end
  end

  describe "save validation" do
  	it 'should save successfully presence' do
  		expect(@mark).to be_valid
  	end
  end
end
