require 'rails_helper'

RSpec.describe Subject, type: :model do
  before(:each) do
  	@subject = Subject.new(subject_name: "name", student_id: "12")
  end

  describe 'validation tests' do
  	it 'ensure subject name presence' do
  		@subject.subject_name = nil
  		expect(@subject).to_not be_valid
  	end

  	it 'ensure student id presence' do
  		@subject.student_id = nil
  		expect(@subject).to_not be_valid
  	end
  end

  describe 'length validation' do
  	it 'should not allow a name longer than 10 character' do
    	@subject.subject_name = "a" *11
    	expect(@subject).to_not be_valid
    end
   end
end
