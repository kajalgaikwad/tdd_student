require 'rails_helper'

RSpec.describe Admin, type: :model do
	before(:each) do
  		@admin = Admin.create!(first_name:'first', last_name:'last', email:'sample@example.com',password:'abcd')
  	end

  describe 'validation tests' do
  	it 'ensure first name presence' do
  		@admin.first_name = nil
  		expect(@admin).to_not be_valid
  	end

  	it 'ensure last name presence' do
  		@admin.last_name = nil
  		expect(@admin).to_not be_valid
  	end

  	it 'ensure email presence' do
  		@admin.email= nil
  		expect(@admin).to_not be_valid
  	end

  	it 'ensure password name presence' do
  		@admin.password = nil
  		expect(@admin).to_not be_valid
  	end
  end

  describe 'length validation' do
  	it 'should not allow a name longer than 10 character' do
    	@admin.first_name = "a" *11
    	expect(@admin).to_not be_valid
    end

    it 'should not allow a name longer than 10 character' do
    	@admin.last_name = "a" *11
    	expect(@admin).to_not be_valid
    end
   end

  describe "save validation" do
  	it 'should save successfully presence' do
  		expect(@admin).to be_valid
  	end
  end
end
