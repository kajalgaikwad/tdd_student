require 'rails_helper'

RSpec.describe Student, type: :model do
	before(:each) do
  		@student = Student.create!(student_name:'name',rollno:'12')
    end
   context 'validation tests' do
  	it 'ensure student name presence' do
  		@student.student_name = nil
  		expect(@student).to_not be_valid
  	end

  	it 'ensure roll number presence' do
  		@student.rollno = nil
  		expect(@student).to_not be_valid
  	end
  end

  describe 'length validation' do
  	it 'should not allow a name longer than 10 character' do
    	@student.student_name = "a" *11
    	expect(@student).to_not be_valid
    end
   end

   describe "save validation" do
  	it 'should save successfully presence' do
  		expect(@student).to be_valid
  	end
  end
end
