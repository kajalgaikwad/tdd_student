class CreateMarks < ActiveRecord::Migration[6.0]
  def change
    create_table :marks do |t|
      t.integer :subject_id
      t.integer :marks

      t.timestamps
    end
  end
end
